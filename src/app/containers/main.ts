import { Component } from '@angular/core'

@Component({
    selector: 'main-container',
    template: `
      <div>
        <main class="main">
        main content will go here
        </main>
      </div>
	  `
})
export class Main { }
